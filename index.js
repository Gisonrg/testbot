'use strict';

// Imports dependencies and set up http server
const
  express = require('express'),
  bodyParser = require('body-parser'),
  app = express().use(bodyParser.json()); // creates express http server

const request = require('request');
const path = require('path');

const TOKEN = "dsajsflgjevi3nflksdlj213";
const PAGE_ACCESS_TOKEN = 'EAAdFhwoTq54BAMaejSiSQgDAfGwckekXINz51dBO61Be3pFP9CHuxnhPqOhcZBnfUKyj3w7kb165Vo5COo2w0sAW1M405wZCXZAI7trxZAlGik3iO35xj9PMaza2acUZBoJX3EfSlJpqT4SYX2eGFHozyUoSZCm7flZAgclLDQWNgZDZD';

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = TOKEN;

  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!')
    } else {
      console.error("Unable to send message:" + err);
    }
  });
}

function handleMessage(sender_psid, received_message) {
  let response;

  // Check if the message contains text
  if (received_message.text) {
    if (received_message.text === 'gpay') {
      response = {
        "attachment": {
          "type": "template",
          "payload": {
            "template_type": "generic",
            "elements": [
              {
                "title": "Welcome to the New GPay Store",
                "image_url": "https://www.valuewalk.com/wp-content/uploads/2018/06/Google-Pay.jpg",
                "subtitle": "GPay for everyone.",
                "default_action": {
                  "type": "web_url",
                  "url": "https://test-messager-app.herokuapp.com/page",
                  "webview_height_ratio": "full",
                  "messenger_extensions": true,
                },
                "buttons": [
                  {
                    "type": "web_url",
                    "url": "https://test-messager-app.herokuapp.com/page",
                    "title": "Start Shopping",
                    "webview_height_ratio": "full",
                    "messenger_extensions": true,
                  }
                ]
              }
            ]
          }
        }
      }
    } else {
      // Create the payload for a basic text message
      response = {
        "text": `You sent the message: "${received_message.text}".`
      }
    }
  }

  // Sends the response message
  callSendAPI(sender_psid, response);
}

// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function (entry) {

      // Gets the message. entry.messaging is an array, but
      // will only ever contain one message, so we get index 0
      let webhook_event = entry.messaging[0];
      console.log(webhook_event);

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;
      console.log('Sender PSID: ' + sender_psid);

      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }

});


// Creates the endpoint for our webhook
app.get('/page', (req, res) => {
  res.header('X-Frame-Options', 'ALLOW-FROM https://www.facebook.com/ https://www.messenger.com/');
  res.sendFile(path.join(__dirname + '/shop.html'));
});

// Sets server port and logs message on success
app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));

